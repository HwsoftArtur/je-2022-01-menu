package org.example.untitled5;

import org.example.untitled5.model.Circle;
import org.example.untitled5.model.Point;
import org.example.untitled5.utils.PointList;


public interface CircleService {
    void setCircle(Circle circle);
    void add(Point point);
    PointList getPointsInCircle();
}
