package org.example.untitled5.model;

import lombok.Value;

@Value
public class Circle {
    Point center;
    double radius;

    public boolean containsPoint(Point point) {
        double distanceToCenter = center.distanceTo(point);
        return distanceToCenter <= radius;
    }
}
