package org.example.untitled5.model;

import lombok.Value;

@Value
public class Point {
    double x;
    double y;

    public double distanceTo(Point point) {
        double deltaX = x - point.x;
        double deltaY = y - point.y;

        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }
}
