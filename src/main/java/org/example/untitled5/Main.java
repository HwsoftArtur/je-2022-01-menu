package org.example.untitled5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.example.untitled5.model.Circle;
import org.example.untitled5.model.Point;
import org.example.untitled5.ui.Menu;
import org.example.untitled5.ui.items.*;
import org.example.untitled5.utils.PointList;

import java.util.Scanner;


//S - Single Responsibility Principal
//O - Open-Closed Principal
//L - Liskov Barbara Principal
//I - Interface Segregation Principal
//D - Dependency Inversion Principal



public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        View view = new View(scanner);
        CircleService circleService = new InMemoryCircleService(
                new PointList(),
                new Circle(new Point(0, 0), 10)
        );

        Menu menu = new Menu(
                new MenuItem[]{
                        new SetCircleMenuItem(circleService,view),
                        new AddPointMenuItem(circleService, view),
                        new ShowPointsInCircleMenuItem(circleService, view),
                        new ExitMenuItem()
                },
                scanner
        );

        menu.run();
    }


}
