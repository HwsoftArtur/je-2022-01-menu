package org.example.untitled5;

import lombok.AllArgsConstructor;
import lombok.Setter;
import org.example.untitled5.model.Circle;
import org.example.untitled5.model.Point;
import org.example.untitled5.utils.PointList;

@AllArgsConstructor
public class InMemoryCircleService implements CircleService {
    private PointList pointList;
    @Setter
    private Circle circle;

    public void add(Point point){
        pointList.add(point);
    }

    public PointList getPointsInCircle(){
        PointList pointListResult = new PointList();
        for (int i = 0; i < pointList.size(); i++) {
            Point point = pointList.get(i);
            if(circle.containsPoint(point)){
                pointListResult.add(point);
            }
        }
        return pointListResult;
    }
}
