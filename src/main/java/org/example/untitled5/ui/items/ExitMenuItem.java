package org.example.untitled5.ui.items;

public class ExitMenuItem implements MenuItem{
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
