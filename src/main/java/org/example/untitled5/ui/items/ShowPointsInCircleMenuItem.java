package org.example.untitled5.ui.items;

import lombok.RequiredArgsConstructor;
import org.example.untitled5.CircleService;
import org.example.untitled5.View;
import org.example.untitled5.model.Point;
import org.example.untitled5.utils.PointList;

@RequiredArgsConstructor
public class ShowPointsInCircleMenuItem implements RunnableMenuItem{
    private final CircleService service;
    private final View view;

    @Override
    public String getName() {
        return "Show points in circle";
    }

    @Override
    public void run() {
        PointList pointList = service.getPointsInCircle();
        view.printPointList(pointList);
    }
}
