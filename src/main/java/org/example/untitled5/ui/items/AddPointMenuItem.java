package org.example.untitled5.ui.items;

import lombok.RequiredArgsConstructor;
import org.example.untitled5.CircleService;
import org.example.untitled5.View;
import org.example.untitled5.model.Point;

@RequiredArgsConstructor
public class AddPointMenuItem implements RunnableMenuItem{
    private final CircleService service;
    private final View view;

    @Override
    public String getName() {
        return "Add point";
    }

    @Override
    public void run() {
        Point p = view.readPoint();
        service.add(p);
    }
}
