package org.example.untitled5.ui.items;

public interface MenuItem {

    String getName();
    default boolean isFinal(){
        return false;
    }
}
