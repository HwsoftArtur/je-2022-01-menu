package org.example.untitled5.ui.items;

import lombok.RequiredArgsConstructor;
import org.example.untitled5.CircleService;
import org.example.untitled5.View;
import org.example.untitled5.model.Circle;

@RequiredArgsConstructor
public class SetCircleMenuItem implements RunnableMenuItem{
    private final CircleService service;
    private final View view;

    @Override
    public String getName() {
        return "Change circle";
    }

    @Override
    public void run() {
        Circle circle = view.readCircle();
        service.setCircle(circle);
    }
}
