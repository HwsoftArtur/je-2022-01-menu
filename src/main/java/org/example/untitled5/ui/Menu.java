package org.example.untitled5.ui;

import lombok.RequiredArgsConstructor;
import org.example.untitled5.ui.items.MenuItem;

import java.util.Scanner;

@RequiredArgsConstructor
public class Menu {
    private final MenuItem[] items;
    private final Scanner scanner;

    private void show() {
        System.out.println("_______________________________________");
        for (int i = 0; i < items.length; i++) {
            System.out.printf("%d - %s\n", i + 1, items[i].getName());
        }
        System.out.println("_______________________________________");
    }

    private int choose() {
        System.out.println("Enter your choose: ");
        int choose = scanner.nextInt();
        scanner.nextLine();
        return choose;
    }

    private boolean runItem(int index) {
        if (!validate(index)) {
            System.out.println("Incorrect input, try again");
            return true;
        }
        run(index);
        return !items[index].isFinal();
    }

    private void run(int index) {
        if(!(items[index] instanceof Runnable)) return;
        Runnable runnable = (Runnable)items[index];
        runnable.run();
    }

    private boolean validate(int index) {
        return index >= 0 && index < items.length;
    }


    public void run() {
        while (true){
            show();
            int index = choose() - 1;
            if(!runItem(index)) break;
        }
    }
}
