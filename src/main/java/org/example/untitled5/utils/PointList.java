package org.example.untitled5.utils;

import org.example.untitled5.model.Point;

import java.util.Arrays;

public class PointList {
    private Point[] points = null;

    public void add(Point point) {
        points = points == null ? new Point[1] : Arrays.copyOf(points, points.length + 1);
        points[points.length - 1] = point;
    }

    public int size() {
        return points==null ? 0 : points.length;
    }

    public Point get(int index) {
        return points[index];
    }
}
