package org.example.untitled5;

import lombok.AllArgsConstructor;
import org.example.untitled5.model.Circle;
import org.example.untitled5.model.Point;
import org.example.untitled5.utils.PointList;

import java.util.Scanner;

@AllArgsConstructor
public class View {
    private Scanner scanner;

    public Point readPoint() {
        System.out.println("Enter x:");
        double x = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Enter y:");
        double y = scanner.nextDouble();
        scanner.nextLine();
        return new Point(x, y);
    }

    public Circle readCircle() {
        System.out.println("Enter the circle center");
        Point center = readPoint();
        System.out.println("Enter the circle radius:");
        double radius = scanner.nextDouble();
        scanner.nextLine();
        return new Circle(center, radius);
    }

    public void printPoint(Point point) {
        System.out.printf("[%.2f,%.2f]\n", point.getX(), point.getY());
    }

    public void printPointList(PointList pointList) {
        if (pointList.size() == 0) {
            System.out.println("empty list");
        }
        for (int i = 0; i < pointList.size(); i++) {
            printPoint(pointList.get(i));
        }
    }
}
