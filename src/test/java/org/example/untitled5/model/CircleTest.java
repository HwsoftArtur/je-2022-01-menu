package org.example.untitled5.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    private Circle circle = new Circle(
            new Point(0,0),
            6
    );

    @Test
    void shouldTestContainsPoint(){
        assertTrue(circle.containsPoint(new Point(0,0)));
        assertTrue(circle.containsPoint(new Point(3,4)));
        assertTrue(circle.containsPoint(new Point(0,6)));
        assertFalse(circle.containsPoint(new Point(9,5)));
    }

}