package org.example.untitled5.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void shouldReturnDistanceBetweenPoints() {
        Point p = new Point(0, 10);

        assertEquals(10, p.distanceTo(new Point(0, 0)));
        assertEquals(5, p.distanceTo(new Point(3, 14)));
    }
}